---
toc: yes
date: 24 Jan 2021
title: "Greenzz: A greener future is in your hands"
subtitle: Projet de groupe
lang: fr
author:
  - Palacios Roosembert
  - Gazzetta Florian
  - Hoang Anh Mai
---


# Problematique

Des nombreuses personnes aiment beaucoup la nature dans leurs espaces mais
n'ont pas forcément la consistance ou le temps nécessaire pour entretenir leurs
petits compagnons racinés.

D'autres plus expérimentées ont plutôt besoin d'un espace de partage où pouvoir
discuter sur une plante en particulier ou s'échanger de boutures ou pépins.

Nous envisageons la création d'un système conçu pour assister ses utilisateurs à
suivre les besoins de leurs plantes de manière consistante et simple
ainsi que la mise en place d'une communauté permettant aux utilisateurs
d'obtenir des conseils pour mieux entretenir leurs plantes mais aussi
permettant un espace d'échange entre utilisateurs expérimentés.

## Description détaillée

**Comment aider les gens dans l'entretien de leurs plantes ?**

Il peut nous arriver d'oublier de prendre soin de nos plantes.
De plus, quelqu'un qui s'intéresse à la botanique découvrira assez rapidement
les besoins différents de chaque espèce de plante, besoins qui peuvent s'avérer
très divers, rendant cette tâche compliquée et chronophage.

Une plateforme mobile qui affiche les informations pertinentes à chaque plante,
ainsi que son état actuel et ses besoins immédiats permettrait à n'importe
quel passionné de diversifier son jardin sans effort.

Cette solution pourra être augmentée à l'aide de cartes à capteurs afin
d'avoir un suivi précis de chaque plante de manière individuelle.

Nous envisageons également de créer un espace communautaire permettant de parler
des ses plantes et leurs défis ainsi que de partager ses connaissances avec
d'autres utilisateurs.

Notre système intégrera également un service premium par abonnement où les
utilisateurs auront à disposition des vues agrégés comme par exemple une vue
agenda qui leurs prévient quelles plantes doivent être arrosées un jour donné,
les plantes qui ont besoin d'un remplacement de terre prochainement ainsi que
des recueils d'informations en début de saison ou autre périodes sur
l'intégralité du jardin.

**Comment créer un espace de partage parmi les amateurs de plantes ?**

Demander des conseils à d'autres utilisateurs en ligne peut s'avérer
épuisant et frustrant lorsque les informations ne peuvent pas être présentées
d'une manière claire et facilement compréhensible.

Notre plateforme propose d'intégrer le suivi d'une plante, ainsi que de pouvoir
le partager avec d'autres utilisateurs ou la communauté entière.
Que ce soit pour exhiber leurs succès ou demander de l'aide, nos utilisateurs
auront le contrôle sur comment et avec qui leurs données et photos seront
partagés.

Si l'utilisateur possède une carte à capteurs, il ou elle pourra choisir de
partager les données disponibles sur notre plateforme afin de recevoir des
conseils plus précis de la communauté.
Cette carte sera disponible chez nos partenaires.

D'autres évènements (comme par exemple, des événements organisées par les clubs
ou magasins locales) pourront être organisées au sein de la communauté.

Nos partenaires pourront mettre à disposition leurs produits afin d'afficher des
recommandations ciblés aux besoins de chaque plante et aux préférences de
chaque utilisateur afin d'augmenter la visibilité de leurs produits.

**Comment fournir un espace virtuel où partager les succès de nos plantes ?**

L'espace communautaire de notre plateforme permettra de découvrir les profils
des plantes d'autres utilisateurs qui auraient publié leurs histoires de
réussite avec leurs plantes.
Cette plateforme permettra d'effectuer des recherches par espèce, région
géographique et popularité et faire des suggestions adaptés à chaque
utilisateur.

## Solution proposée

Nous proposons **Greenzz**: un système conçu pour assister ses utilisateurs à
être régulier et écouter les besoins de leurs plantes ainsi que fournir des
conseils pour mieux les entretenir.

Ce système est divisé en quatre composantes :

- Une _application mobile_ gratuite où ses utilisateurs peuvent faire un suivi
  de leurs plantes avec des photos journalières ainsi que recevoir des
  informations pertinentes à chaque plante.

- Une carte à capteurs optionnelle qui peut être installé à côté de chaque
  plante afin de fournir des recommandations en temps réel, mesurer et suivre
  l'évolution de la plante et finalement permettre à l'application de notifier
  l'utilisateur des besoins non comblés de ses plantes.

- Un service en ligne avec diverses informations sur ces plantes, consultable à
  travers l'application.
  Ce service est aussi responsable de coordonner l'application mobile avec la
  carte à capteurs, si celle-ci est installée.

- Un espace communautaire qui permettra de mettre en vedette ses plantes et
  réagir aux profils d'autres plantes, ainsi que de permettre aux utilisateurs
  plus expérimentés de venir au secours d'autres plantes en détresse mais aussi
  d'organiser des échanges de boutures ou autres événements locales.

## Modèle d'affaires (business model)

### Partenaires clés

- Magasins de jardinerie : Promouvoir les produits adaptés aux besoins de chaque
  plante et aux préférences de chaque utilisateur à disposition dans les
  magasins de jardinerie près de l'utilisateur.
  Vente de notre carte à capteurs.

- Marchés botaniques : Activation de la communauté, évènements pour attirer des
  utilisateurs, activités pour maintenir la communauté engagée.

- Communauté des amateurs des plantes : Fidélisation de la clientèle à travers
  des activités en ligne.

- Hébergeur des services en ligne : Au vu de son importance et de la nature
  sensible des données des utilisateurs, il est primordial de choisir un
  partenaire en Suisse avec des produits fiables. [Infomaniak][] et [Exoscale][]
  nous semble être un choix judicieux.
  Leurs solutions proposent de plus une mise en route simplifiée pour
  rapidement obtenir une identité numérique et une plateforme adéquate à
  l'exploitation de services en ligne respectivement.

[Infomaniak]: https://infomaniak.ch
[Exoscale]: https://exoscale.ch

### Activités clés

- Développement d'une plateforme pour le suivi des plantes (application, site
  internet, communauté) : Principal moyen de communication avec la clientèle.

- Développement d'une carte à capteurs : Recherche et développement
  d'indicateurs clés pour le suivi des plantes.

- Maintenance d'une communauté en ligne : Modération, animation et fidélisation
  de la clientèle.

### Ressources clés

- Application mobile : D'après l'OFS ([source][ofs-accès-internet]), 98.5 ± 0.6%
  de la population entre 30 et 59 ans et 82.0 ± 2.4% de la population de plus de
  60 ans avec accès internet le font à travers un smartphone, soit 3 645 865
  personnes (30-59 ans) et 1 649 152 (60 ans et plus).
  Grâce aux technologies existantes, nous pourrons développer des applications
  pour la plupart de systèmes d'exploitation de manière multi-plateforme, ce
  qui contribuera à réduire énormément les coûts de développement.
  Cette application nous permettra d'atteindre directement nos utilisateurs
  sans dépendre d'autres intermédiaires.

[ofs-accès-internet]: https://www.pxweb.bfs.admin.ch/pxweb/fr/px-x-1604000000_106/-/px-x-1604000000_106.px

- Communauté en ligne : La communauté a une fonction de service aux
  utilisateurs, mais sert aussi à garder les utilisateurs engagés au produit.

- Constructeurs des cartes à capteurs : La carte à capteurs offre un service
  premium aux utilisateurs, avec des données en temps réel accessibles à travers
  l'application à tout moment.

### Propositions de valeur

- Suivi simplifié de l'évolution de ses plantes.
- Conseils à l'utilisateur sur les besoins de ses plantes.
- Affichage de produits des nos partenaires adaptées aux besoins de leurs
  plantes ainsi qu'aux préférences de l'utilisateur.
- Aider l'utilisateur a suivre de manière consistante les besoins de ses plantes.
- Carte capteurs pour un suivi détaillé de l'état de la plante ainsi que des
  recommandations de produits partenaires dont cette plante pourrait bénéficier.
- Espace communautaire (mise en vedette & demandes d'aide).
- Fonctionnalité « premium » sous abonnement : Vue agrégé sur l'ensemble du
  jardin avec des informations détaillées sur les tâches récurrentes ainsi que
  des stratégies pour simplifier l'entretien mais aussi rester informé des
  tâches moins fréquentes à venir.

### Relations avec les clients

- Forum communautaire.
- Support de la carte à capteurs via nos parternaires.

### Canaux

- Distribution de l'application mobile sur divers app stores :
  Ceci augmente la visibilité de l'application mobile, qui est le point d'entrée
  de nous utilisateurs.

- Vente de carte à capteurs sur les boutiques partenaires : Ceci nous permet de
  réutiliser l'infrastructure existante pour la distribution et service après
  vente (et support technique) des produits déjà en place afin de réduire nos
  coûts d'exploitation ainsi que le stock.

- Plateforme en ligne à travers l'application : Le principal moyen d'interaction
  des utilisateurs sera la section communautaire de l'application, nous
  permettant de rester connectés avec la communauté, mais aussi de réduire les
  coût du service à la clientèle.

### Catégories de clients

Notre modèle d'affaires comprend des partenariats avec des magasins de
jardinerie locaux ainsi que des utilisateurs de notre application.

Nous ciblons un publique adulte jeune (22 ans à 30 ans), des adultes avec
des familles avec des enfants jeunes (35 ans à 50 ans) ainsi que des
retraités (60 ans et plus).

Dans les cas des adules jeunes, nous nous attendons beaucoup moins de
revenus directe, mais beaucoup de participation dans la communauté.

Dans la tranche adultes avec familles et retraités, nous estimons que cette
clientèle sera fortement attiré par nos fonctionnalités « pro »
(détaillées dans la section produit > gamme) où la sérénité apporté par
l'application grâce aux rappels ainsi qu'aux recommandations stratégiques pour
l'entretien du jardin permettront d'une part aux adules en famille de gérer un
jardin de moyenne grandeur (20-30 plantes) éparpillés dans leurs maison sans
avoir à se soucier ni engager du temps sur de chacune de ces plantes de manière
individuelle; d'autre part, les retraités pourront eux s'engager dans la
culture de jardins plus divers nourris possiblement de plantes avec des
besoins plus spécifiques ou engageants.

Dans ces catégories de clients, nous estimons pouvoir atteindre un segment de
clientèle d'environ 5 132 638 personnes (Voir section _public cible >
segmentation_ pour les détails de ces chiffres).

### Structure des coûts

- Services informatiques liés à l'exploitation de l'entreprise, ainsi qu'à
  l'image et identité numérique : CHF 150.-/an (Infomaniak web + mail).

- Exploitation des services en ligne : Plateforme en ligne et stockage des
  données. CHF 1000.-/an (Exoscale 8GB, 4 core, 300G SSD).

- Maintenance et gestion de la communauté : Animations, activités de modération
  et fidélisation de la clientèle (e.g. concours). CHF 24 000.-/an
  (employé 50%).

- Développement : Application mobile, carte à capteurs, services en ligne.
  CHF 4000.-/an (Outils de développement matériel, prototypes, téléphones et
  licences des logiciels).

- Fabrication des cartes à capteurs.

- Salariés : Développeurs, exploitation du service, gestion de la communauté.

### Flux de revenus

- Abonnements « Pro » (voir section _Produit > Gamme_).

- Vente de cartes à capteurs à travers nos partenaires : Dans l'optique d'éviter
  les coûts de stockage et vente et afin d'attirer le plus de partenaires
  possible, nous proposons la vente de la carte à capteurs en consignation.

- Partenariat avec magasins de jardinerie : Nos utilisateurs auront des
  recommandations de produits de nos magasins parternaires, ainsi que des
  liens pour l'achat direct avec possibilité de rejoindre un programme de
  fidélisation.
  Nos partenaires bénéficieront ainsi non seulement d'une augmentation de leurs
  ventes, mais aussi d'informations stratégiques sur les plantes les plus
  populaires de la région et les préférences des utilisateurs, avec possibilité
  de la mise en vedette des nouveautés au sein de l'application.

## Forme juridique

Nous proposons la création d'une SARL dans le but de diminuer les risques pour
ses fondateurs, mais aussi d'avoir le capital nécessaire pour s'introduire dans
le marché et assurer ses activités clés lors de la phase de création de la
communauté.

<!-- TODO: Genre économique -->

<!-- TODO: But -->

# Analyse

## Externe

### PESTEL

#### Politique

Nous estimons que notre produit n'est pas touché par le contexte politique
suisse.
Si bien, l'activisme écologique est très actif en Suisse, celui-ci n'a aucun
impacte sur notre produit.

Un aspect important auquel il faudrait faire attention est l'importation et la mise en circulation de végétaux interdits à la cultivation. Il ne faudrait également pas que notre produit incite aux utilisateurs à ramener des végétaux non réglementaires au niveau sanitaire qui présentent des risques à la flore locale.

#### Économique

D'après l'OFS ([source][ofs-e-commerce-produits-achetés]), en 2021, environ
15.4 ± 1.8 % de la population entre 30 et 59 ans (soit 568 297 ± 66 166
personnes) ainsi que 4.1 ± 1.3 % de la population de 60 ans et plus (soit
82 196 ± 26 324 personnes) ont acheté des logiciels payants (exclus les jeux
vidéo, logiciels pour ordinateurs ainsi que les applications liées à la santé).

[ofs-e-commerce-produits-achetés]: https://www.pxweb.bfs.admin.ch/pxweb/fr/px-x-1604000000_107/-/px-x-1604000000_107.px

Afin de pouvoir proposer la carte à capteurs à des prix compétitifs, il faut
minimiser son prix de manufacture.
Nous avons constaté avec notre prototype que l'utilisation de composantes
standard peut grandement réduire les coûts des matières premières au détriment
des dimensions du produit et complexité de conception du processus d'assemblage.

Ces caractéristiques pourront être mises en avant lors de la commercialisation
de la carte à capteurs pour soulager la pression sociale face à l'augmentation
de déchets électroniques.

Avec l'arrivée de la pandémie, les gens ont tendance à ajouter de la verdure chez eux en achetant des plantes. Ces derniers sont souvent locaux, comme une [étude](https://www.rts.ch/info/suisse/11732666-les-ventes-de-fleurs-et-de-plantes-dinterieur-explosent-avec-le-covid.html) l'a constatée. Notre produit pourrait encourager les personnes à en acquérir davantage, ce qui favorise l'augmentation de l'économie locale.

#### Socioculturels

En conséquence des normes d'isolation et de la réduction des activités sociales
conséquentes, la proposition d'augmenter la surface d'interactions avec des
inconnus qui partagent non seulement les mêmes passions, mais aussi
le même environnent local socioculturel peut apporter une valeur ajoutée qui
n'est pour le moment que très peu répandue sur les espaces d'échanges numériques
existants.

La création et commercialisation de la carte à capteurs peut engendrer une
hausse de la pollution liée au paquetage, et l'arrivée en fin de vie du produit
une hausse des déchets électroniques.
Ces conséquences pourraient être fortement critiquées par nos clients et
l'opinion publique, surtout avec les mouvements de sensibilisation aux déchets
d'équipements électriques et électroniques et raison de la production de déchets
électroniques d'autres secteurs.

Cependant, l'utilisation de modules électroniques standards peut limiter non
seulement la quantité de déchets électroniques générés en fin de vie du produit,
mais aussi les coûts de production.
Il est envisageable de concevoir un programme de récupération des
anciennes cartes à capteurs en bon état afin de désassembler et potentiellement
réutiliser ou revendre les capteurs et unités de contrôle.
Ceci entraîne dans tous les cas des coûts supplémentaires qui pourraient en
partie être subventionnés par nos clients de manière volontaire, par des
organes publics ou à travers des partenaires spécialisés tels que [eRecycling][].

[eRecycling]: https://www.erecycling.ch/

Nous estimons qu'en général la population Suisse est particulièrement sensible
aux problèmes écologiques est voudront contribuer aux frais de traitement des
déchets.
Cet engagement publique envers le recyclable nous permettra d'avantage d'assurer
une opinion positive de notre produit de la part de notre clientèle.

Il est également prouvé que posséder des plantes chez soi améliorent l'humeur car les humains ont besoin d'un peu de nature dans leur quotidien.

#### Technologique

La haute disponibilité des téléphones mobiles (92% des adultes en Suisse.
[source][dispo-smartphone-suisse]) nous fournit une grande surface de marché
de clients potentiels.

[dispo-smartphone-suisse]: https://www2.deloitte.com/ch/fr/pages/press-releases/articles/deloitte-in-switzerland-smartphones-become-control-centre.html

La pénurie des composants électroniques au niveau mondial
([source][chip-shortage]) peut augmenter les prix de manufacture des
cartes à capteurs.
Néanmoins, la réutilisation de modules électroniques standards peut limiter
les coûts liés à la manufacture des produits électroniques dédiés sans impact
sur la qualité ou la fonctionnalité de la carte.

[chip-shortage]: https://eps-spe.com/penurie-de-composants-electroniques-quel-horizon-pour-2022/

#### Écologique

Si bien l'utilisation de smartphones est responsable d'une quantité
non négligeable de consommation énergétique au niveau mondial
([source][énérgie-smartphones]), l'impact de notre application dans le milieu
est une goute d'eau dans l'océan d'applications déjà à disposition des
utilisateurs.

[énérgie-smartphones]: https://www.dossierfamilial.com/environnement/transition-energetique/le-lourd-bilan-carbone-des-smartphones-et-ordinateurs-431782

En contrepartie, la création et commercialisation de la carte à capteurs peut
engendrer une hausse de la pollution liée au paquetage, et l'arrivée en fin de
vie du produit une hausse des déchets électroniques.
Cependant, l'utilisation de modules électroniques standards peut limiter non
seulement les coûts de production, mais aussi la quantité de déchets
électroniques générés en fin de vie du produit.

Il est envisageable de concevoir un programme de récupération des
anciennes cartes à capteurs en bon état afin de désassembler et potentiellement
réutiliser ou revendre les capteurs et unités de contrôle.
Ceci entraîne dans tous les cas des coûts supplémentaires qui pourraient en
partie être subventionnés par nos clients de manière volontaire, par des
organes publics ou à travers des partenaires spécialisés tels que [eRecycling][].

[eRecycling]: https://www.erecycling.ch/

Nous risquons d'impacter l'écologie négativement à cause du transport des cartes à capteurs du centre de production jusqu'à nos locaux. A moins d'utiliser les services d'un transporteur carburant entièrement à l'énergie renouvelable, il y a des fortes chances que ce soit de l'énergie fossile qui sera choisi.
Cependant notre produit a le potentiel de promouvoir la biodiversité et à créer davantage d'espaces verts car la maintenance des plantes sera facilitée.

Finalement, nous visons à utiliser un paquetage pour les cartes à capteurs en
carton recyclable et y inclure un code QR pour lancer l'application qui guidera
l'utilisateur dans l'utilisation de la carte au lieu d'inclure un manuel
d'utilisation en papier.

#### Légal

Notre secteur de marché n'est majoritairement pas régulé.
Il existent néanmoins quelques régulations liés à l'exploitation de services
informatiques qui sont particulièrement sensibles dont il faut rester
particulièrement attentif.

- Régulations liées au traitement des données. Des mesures afin de rester
  conforme aux lois Suisses sur la protection des données ([LPD][]), ainsi
  qu'aux normes européennes ([GDPR][]) devront êtres prises.

[LPD]: https://www.edoeb.admin.ch/edoeb/fr/home/protection-des-donnees/generalites/protection-des-donnees.html
[GDPR]: https://gdpr.eu/

- Modération du contenu créé par la communauté.
  Des stratégies pour modérer le contenu à disposition sur l'espace
  communautaire devront être adaptées afin de protéger l'image de la marque,
  mais aussi afin de mitiger le risque de contenu illégal sous risque de
  poursuites ou de retrait forcé du marché.

### Analyse du secteur

#### Demande

Il est particulièrement difficile de chiffrer l'intérêt de la population Suisse
sur les plantes et les jardins.
On peut néanmoins constater qu'un marché d'amateurs et passionnés des plantes
existe, puisqu'il existe de grosses chaines de magasins proposant des plantes et
des produits liés tels que _Coop Brico + Loisir_ avec plus de 70 succursales au
niveau national, sans mentionner les magasin locaux.

Aucun de ces magasins ne fournit de stratégie pour faire un suivi de l'évolution
de ses plantes, demander de conseils ou gérer une communauté.

#### Offre

Une recherche sur internet révèle l'existence de capteurs avec des buts
similaires dans le marché à un prix proche de 30 CHF (hors frais de ports).
Notre solution a l'avantage de fournir non seulement une intégration avec
notre application mobile qui permet de faire un suivi en plus de mesures,
mais aussi le partage de ces données et profils avec la communauté.

Des nombreux ressources sont disponibles sur internet sur comment prendre soin
des plantes en général ainsi que des plantes spécifiques, mais chercher, filtrer
les informations pertinentes est une tâche extrêmement chronophage.
Aucune des solutions que nous avons trouvé a la possibilité d'enregistrer notre
inventaire de plantes et encore moins recevoir que les informations pertinentes
au moment ou d'une manière structuré.
Notre solution non seulement nous permet d'accéder aux informations pertinentes
de chaque plante sur notre jardin sur le moment, mais aussi les présenter d'une
manière claire et intuitive en montrant précisément ce que l'utilisateur doit
faire.

Des nombreuses communautés sont disponibles en ligne, mais très peu ont une
intégration locale (pour, par exemple, l'échange de boutures) et les gens
doivent jongler entre les communautés au niveau mondial (et donc souvent pas en
français ou adaptés aux variétés et contexte local), les organisations locales
(comme par exemple, les associations aux campus) et le travail assidu de
chercher sur internet.
Ces solutions sont simplement inaccessibles pour quelqu'un qui ne peut ou ne
veux pas engager son temps et/ou énergie.

Il faudrait néanmoins surveiller l'évolution des capteurs concurrents. En effet, les capteurs sont notamment proposés par un grand groupe chinois, possédant une grosse clientèle fidèle. Cette entreprise est de plus très développée. Elle possède donc un grand nombre d'employés et de budget la permettant de mieux répondre aux besoins des clients.

#### Intensité concurrentielle

Quelques applications mobiles avec des fonctionnalités similaires, existent
dans le marché global.
Ces applications se concentrent sur les plantes de manière individuelle sans
pour autant donner des informations pertinentes sur le moment.
Nos concurrents proposent aussi de l'identification automatique de l'espèce de
plantes; néanmoins nous avons remarqué que ces applications ont une quantité
significative de commentaires de ses utilisateurs qui se plaignent de
l'imprécision de cette fonctionnalité.

Aucun des concurrents que nous avons trouvé (applications de type galerie de
plantes ainsi que capteurs similaires au notre) ne propose aucun sorte d'espace
communautaire où ses membres puissent publier leurs de plantes et voir celle des
autres; ni d'intégration avec les marchés locaux.

#### Groupes stratégiques

Objectifs stratégiques :

- 50k utilisateurs actifs par mois sur notre plateforme à la fin de la première
  année.

- Une sous-communauté régionale par chef-lieu de chaque canton en Suisse
  romande.

- 5k abonnements « Pro » sur la première année après le lancement.

- 500 cartes à capteurs vendues sur la première année après le lancement.

- 50 partenariats à la fin de la première année.

### Forces de PORTER

#### Pouvoir de négociation des clients

Clients:

- Partenaires (magasins de jardinerie) :

  D'une part, l'un de nos flux de revenus principales étant nos partenaires,
  ceux-ci sont en position de force.
  D'autre part, la plupart de nos partenaires sont locaux, nous rendant moins
  dépendants de chaque partenaire de manière individuelle; néanmoins, le risque
  que ces magasins cherchent à proposer des produits concurrent n'est pas
  négligeable.
  Nos partenaires génèrent non seulement des revenus liés aux cartes à
  capteurs, mais aussi aux partenariats contre de la visibilité sur notre
  application; généralement les moyens des magasins de jardinerie locaux ou
  régionaux d'atteindre leurs clientèle sont limités aux moyens de
  communication de masses (e.g. Radio, TV) ou bouche à oreille.
  Par conséquent, notre proposition afin qu'ils puissent atteindre leur
  clientèle de manière précise pour attirer de nouveaux clients ou annoncer
  de nouveaux produits est assez unique.

  D'autres chaînes de partenaires plus répandues (comme par exemple,
  _Coop Brico+Loisir_), qui ont un impact plus important dû à leur présence
  au niveau national, ont un pouvoir de négociation plus important et
  pourraient chercher à négocier un bénéfice plus élevé ou bien essayer
  d'influencer notre plateforme.

- Clients directes :

  Étant donné que nous ne proposons que des services individuels à chaque
  utilisateur, la force de négociation d'un utilisateur individuel est assez
  limité. Or, nous ne voyons pas l'intérêt commercial de supporter des « packs
  d'abonnements » donc nous estimons très peu probable avoir à négocier avec
  des collectifs d'utilisateurs.

#### Pouvoir de négociation des fournisseurs

- Distributeurs de l'application mobile :

  Nous sommes entièrement dépendants des distributeurs des applications mobiles
  des plus grandes plateformes.
  De plus, ils tiennent un monopole sur chaque plateforme sans contestation.

- Hébergeurs des services en ligne :

  Nous sommes entièrement dépendant des fournisseurs de la plateforme nécessaire
  pour l'exploitation de services en ligne.
  Néanmoins, à l'échelle de nos besoins, nous ne sommes absolument pas un client
  particulièrement important pour eux.

  Dans le cas où on souhaiterait changer de fournisseur (soit chez un autre,
  soit maintenir des serveur physiques nous même) :
  D'une part, changer de fournisseur de plateforme de services en ligne n'est
  généralement pas compliqué et requiert de quelques heures seulement;
  D'autre part, la disponibilité du service pour nos utilisateurs ne pourra pas
  être assurée pendant la période de migration. Ce qui aura potentiellement un
  impacte négatif sur la confiance de nos utilisateurs et réputation de la
  marque.

- Fabricants des pièces de la carte à capteurs :

  L'utilisation de composants standardisés nous permet de rester assez
  indépendants des fournisseur des composants des cartes à capteurs, ce qui nous
  permet également une flexibilité sur les technologies qui évoluent au cours du
  temps sans avoir de coûts significatifs en termes de développement
  logiciel pour s'adapter à un autre fournisseur de ces modules.

  Étant donné qu'une petite partie des pièces nécessaires à la construction de
  la carte à capteurs, bien que très simples, doivent quand même être produites
  sur mesure, un changement de fournisseur entrainerait un certain coût
  surtout en termes de temps.

#### Intensité concurrentielle

Aucun de nos concurrents propose actuellement une solution équivalente à nos
produits.

On pourrait s'attendre à ce que l'un de nos concurrents décide de s'étendre
vers notre modèle d'affaires (en particulier de grandes entreprises de
capteurs comme par exemple _Honeywell_), mais nous estimons que ce genre de
concurrent global aura plus de peine à entrer dans les marchés locaux suisses.

Bien qu'il soit possible que des magasins de jardinerie commencent à proposer
des aspects communautaires, nous estimons que ceux-ci seront difficilement
intégrés avec des capteurs déjà existants dans le marché au vu de leur pouvoir
de négociation et de la taille du marché qu'ils pourraient atteindre.
Finalement, il n'est pas exclu qu'un concurrent Suisse propose une solution
similaire, mais à supposer que notre clientèle soit partout en Suisse, nous
estimons que le marché pourrait tolérer plusieurs concurrents avant d'arriver
à saturation.

#### Menace des produits de substitution

Parmi les possibles futurs concurrents précédemment décrits, il n'est pas exclu
que des produits similaires (p.ex. des pots connectés) entrent dans le marché.
Notre conclusion est que ce scénario serait équivalent à avoir un concurrent
direct, comme ceux précédemment décrits. De plus, si ces concurrents proviennent d'un pays possédant un niveau de vie plus bas que la Suisse, ils auront l'avantage de posséder une main d'oeuvre à un coût moindre. Ils ont donc les moyens de proposer des produits à petits prix ce qui peut poser un danger pour notre produit.

#### Menace des nouveaux entrants

Un éventuel nouveau concurrent devra pouvoir fournir uns application mobile (ou
un autre moyen de connecter avec ses utilisateurs), développer une carte à
capteurs, trouver des partenaires ainsi que créer une communauté.

Bien qu'il ne soit pas impossible qu'une partie de notre communauté migre vers
la concurrence, il y a peu de chances que cette migration soit systématique,
chaque sous-communauté étant locale.

L'engagement de nos utilisateurs sur l'application joue de plus un rôle
fondamental dans la fidélisation de nos clients (qui ne veulent pas
perdre l'historique et l'évolution de leurs plantes).

#### Pouvoir des autorités

Notre marché n'est pas particulièrement réglementé.
Quelques réglementations spécifiques aux services en ligne s'appliquent, mais
ne sont pas spécifiques à notre marché : ([LPD][], [GDPR][]).

## Interne

### Compétences

![Matrice des compétences](images/competences.png)

Comme nous pouvons le voir dans la répartion des compétence, les différents membres du projets possèdent de forte compétences techniques, ainsi que quelques compétences commerciales mais manquent de compétences de gestion d'équipe ainsi que pour les question légales et administratives, c'est pourquoi nous avons envisagé de recruter un chef de projet qui serait externe au projet. En plus de cela, il nous manque également les compétences pour gérer la communautée. C'est pourquoi nous allons également recruter (à mi-temps) une personne qui s'occupera de gerer la communautée.

### Chaîne de valeur de Porter

#### Activités de soutien

- Gestion des ressources humaines : Un employé pour l'assemblage des cartes à
  capteurs et gestion de la logistique. Un employé pour le développement et
  exploitation de la plateforme. Un employé pour la gestion de la communauté.
  Fiduciaire pour la gestion des employés et payements.

- Approvisionnements : Les outils nécessaire pour le développement sont
  majoritairement des logiciels libres. Recherche de partenaires.

- Développement technologique : Études de marché pour attirer la clientèle,
  recherche d'indicateurs ou fonctionnalités à valeur ajoutée pour améliorer les
  informations présentées aux utilisateurs. Maintenance des bases de données de
  plantes.

- Infrastructure de l'entreprise : Locaux pour l'assemblage, paquetage et
  stockage des cartes à capteurs.
  Serveurs pour l'exploitation des services en ligne.

#### Activités principales

- Logistique interne : Réception et stockage des composantes de la carte à
  capteurs.

- Production : Assemblage, empaquetage et stockage des cartes à capteurs prêtes
  à l'emploi.
  Développement de la carte à capteurs, de l'application mobile et
  des services en ligne (e.g. espace communautaire).

- Logistique externe : Livraison des cartes à capteurs aux partenaires.

- Commercialisation et vente : À travers nos partenaires. Publicités. Présence
  aux marchés botaniques. Fidélisation de la clientèle (e.g. concours).

- Services : Service client aux fournisseurs. Gestion de l'espace communautaire.

#### Marge

- Communauté.

- Cartes à capteurs de bonne qualité.

## SWOT

#### Forces

- Projet innovateur, dans un marché avec peu de concurrents.
- L'application mobile gratuite rend le système très accessible.
- L'intégration des magasins locaux promeut les économies locales.
- L'espace communautaire apporte une solution accessible aux utilisateurs.

#### Faiblesses

- Le développement de la carte à capteurs est coûteux pour une section réduite
  des utilisateurs.
- La communauté sur laquelle la viabilité du produit repose doit être créée,
  qui peut s'avérer difficile.
- Image de marque à créer.
- La logistique des produits matériels peut avoir un coût très élevé.
- Difficulté à convaincre les utilisateurs d'acheter un produit matériel.

#### Opportunités

- Pas de solution comparable.
- Utilisation des plateformes numériques et des communautés en ligne exacerbée
  par la crise du COVID-19.
- Réception très positive de l'opinion publique.
- Peu de solutions pour augmenter la visibilité de magasins de jardinerie
  locaux de manière ciblé et adapté à chaque individu.

#### Menaces

- Arrivée possible de nouveaux concurrents.
- Hausse dans les prix de manufacture de la carte à capteurs.
- Durcissement des régulations liées à la collecte et stockage des données
  personnels et liés à la vie privée.
- Abus de l'espace communautaire par des utilisateurs malveillants.

<!-- TODO: Justifié les stratégies choisies. -->

## Public cible

### Segmentation

Notre produit vise à unifier différentes parties d'un marché segmenté :
d'une part, des utilisateurs expérimentés qui exposent les résultats de leur
travail sur leurs plantes et qui ont besoin d'un espace de partage pour discuter
de certaines plantes en particulier ainsi que pour le partage des boutures ou
pépins;
d'autre part, des utilisateurs amateurs qui demandent de l'aide sur la manière
de prendre soin de leurs plantes.

Nos partenaires gagneront de même en visibilité grâce aux suggestions de
produits adaptées individuellement à chaque plante que l'utilisateur possède
ainsi qu'au feedback sur les produits achetés par d'autres utilisateurs.
Nous envisageons que la plupart de partenaires seront de magasins locaux,
mais aussi, en plus petite quantité, des chaînes régionales ou nationales.
Ces partenaires pourront ainsi offrir de la visibilité à de nouveaux produits.

Les achats sont réalisés par nos utilisateurs chez nos partenaires : que ce soit
la carte à capteurs ou des produits du magasin. Des accords commerciaux pourront
être prévus chez nos partenaires afin de créer des promotions, événements ou
autres.

Tel que précédemment expliqué dans la section _modèles d'affaires > catégories
de clients_, nos principaux consommateurs directes correspondent aux personnes
entre 35 à 50 et 60 et plus ans.
D'après l'OFS ([source][bilan-demographique-2020]), le segment de la population
entre 22 et 30 ans effectif au 1er janvier 2020 en Suisse représente 954 532
individus alors que le segment d'individus entre 35 et 50 ans est de 1 938 373,
finalement le segment de 60 ans et plus représente 2 239 733 individus.
Ce qui nous donne un segment de clientèle d'environ 5 132 638 personnes avec
un total de 4 178 106 potentiels consommateurs directes.

[bilan-demographique-2020]: https://www.pxweb.bfs.admin.ch/pxweb/fr/px-x-0102020000_103/-/px-x-0102020000_103.px

### Ciblage

- Utilisateurs entre 22 et 30 ans: Promotion de la marque sur les stores
  d'applications mobiles ainsi de chez des _influenceurs_ des réseaux sociaux.
  Pourvu que l'on retrouve un concentration particulière de ce segment de
  clientèle dans les universités et hautes écoles, des évènements près des
  campus seront clés pour attirer ce public.

- Ménage avec enfants (35 à 50 ans) & retraités (60 ans et plus):
  Promotion de la marque chez nos partenaires. Bouche à oreille à travers de
  nos utilisateurs plus jeunes. Communautés en ligne et régionaux d'amateurs
  de plantes. Site internet. Afin de motiver les communautés d'amateurs de
  plantes à parler de notre marque, l'on pourra organiser des concours et
  autres événements avec le soutien de nos partenaires.

Des éventuelles promotions de saison pourront être envisagés à travers nos
partenaires afin d'attirer plus de clientèle directe.

### Positionnement

Nous considérons _Greenzz_ comme un assistant personnalisé qui accompagne
ses utilisateurs et leurs plantes, mais aussi une plateforme de
partage d'histoires de ses utilisateurs avec pour but de devenir le portail
préféré des amateurs des plantes.

_Greenzz_ est disponible de manière gratuite (si bien limitée sur ses
fonctionnalités) à tous ses utilisateurs.
Notre plateforme propose un abonnement « Pro » payant (1 CHF par mois /
10 CHF par an) qui retire toutes les limitations ainsi que fournir des
fonctionnalités plus avancées.

_Greenzz_ permet à nos partenaires d'atteindre leur publique cible.
Notre plateforme propose plusieurs modèles de partenariat à partir de CHF 75:
_entry_, _standard_ et _gold_ en fonction du nombre d'utilisateurs visés
et le nombre de produits promus par l'application. (Voir section _Produits >
Partenariats_).

# Produits

## Application mobile

_Greenzz_, une application mobile gratuite qui permet de suivre les besoins
d'une plante et d'assister l'utilisateur par moyen de rappels, de notifications
et de conseils d'autres utilisateurs pour mieux l'entretenir.
Cette application permet à ses utilisateurs de :

- Enregistrer chacune de ses plantes dans l'application et stocker une photo par
  jour de chaque plante afin de suivre l'évolution de celle-ci.

- Afficher des informations pertinentes à l'espèce de plante ainsi de des
  conseils généraux pour en prendre soin.

- Si l'utilisateur possède une carte à capteurs, la synchroniser via
  l'application afin de monitorer l'état de la plante (une plante par capteur)
  et afficher des indicateurs en temps réel ainsi que des informations agrégées
  sur une longue période.

- Donner des recommandations sur l'entretien adéquat de chaque plante.

- Rappel sur les opérations d'entretien de la plante (arrosage, repotage,
  taille, ...).

- Création d'un planning plus engagé pour les utilisateurs avancés (e.g. rappel
  de suppléments minéraux, spray tous les 4 mois, ...).

- Rappel de prendre des photos journalières de chaque plante.

- Si l'utilisateur possède une carte à capteurs, émettre des notifications ou
  alertes lorsque la plante n'est pas sous conditions optimales (e.g.
  température, lumière, arrosage).

- Donner des recommandations sur des produits pouvant aider chaque plante
  disponibles chez des magasins partenaires.

- Accès à l'espace communautaire. Les utilisateurs pourront découvrir, regarder,
  commenter ainsi que suivre des profils de plantes ou d'autres utilisateurs.

- Publication d'un profil de plante (avec l'historique des photos) sur l'espace
  communautaire. Si une carte à capteurs est installée, ces données peuvent
  optionnellement être partagées.

- Évènements et concours dans l'espace communautaire.

- Demande d'aide sur l'entretien d'une plante sur l'espace communautaire.

- Affichage des produits des partenaires dans l'espace communautaire.

### Gamme

L'application mobile aura trois gammes de service :

- « Access » : L'utilisateur installe l'application, ajoute jusqu'à 10 profils
  de plantes et peut prendre des photos journalières de ses plantes.
  L'utilisateur peut visualiser l'espace communautaire, mais ne peux pas
  interagir avec lui.
  L'utilisateur n'est pas identifié, ne possède pas de compte et aucune
  information n'est sauvegardé sur le cloud.
  L'utilisateur n'est pas au courant qu'il profite de la gamme « access », mais
  voit un message plusieurs fois par mois l'encourageant à créer un compte.
  L'utilisateur ne peut pas ajouter une carte à capteurs.

- « Basic » : L'utilisateur crée un compte gratuit.
  Le tag 'basic account' s'affiche sur son profil.
  Il peut ajouter jusqu'à 20 profils de plantes et sauvegarder ses profils
  ainsi qu'un an d'historique de chaque plante sur le cloud.
  Il peut ajouter des cartes à capteurs et interagir avec la communauté.
  L'utilisateur peut laisser jusqu'à 20 « J'aime » sur des photos par jour.
  L'utilisateur peut suivre d'autres utilisateurs mais il ne peut ajouter que 5
  utilisateurs à suivre par jour.
  L'utilisateur peut accéder à la liste des photos sur lesquelles il a mis un
  « J'aime » et voit une section avec les nouvelles photos d'autres utilisateurs
  qu'il suit.

- « Pro » : L'utilisateur prend un abonnement 1.- CHF/mois ou 10.- CHF/an.
  Le tag 'Pro account' s'affiche sur son profil.
  Il peut ajouter autant de profils de plantes qu'il veut.
  L'historique des plantes stocké sur le cloud est illimité.
  L'utilisateur peut laisser autant de « J'aime » et suivre autant
  d'utilisateurs qu'il veut.
  L'utilisateur peut cacher les produits des partenaires s'il le veut.
  L'utilisateur a accès à des vues agrégés sur son jardin: Les plantes qui
  doivent être arrosées un jour donnée, les plantes qui ont besoin de remplacer
  la terre, des recueils d'informations en début de saisons (e.g. les plantes
  qui ne doivent pas être arrosées en hiver).


### Marque

Notre position de marque sera poussée sur l'accompagnement et le suivi des
plantes ainsi que le partage des histoires et émotions que ses utilisateurs
éprouvent au travers de leurs plantes.

## Carte à capteurs

_Greenzz sensor board_, disponible à 30.- CHF, cette carte à capteurs
permet le suivi détaillé des besoins d'une plante.
Cette carte apporte les fonctionnalités premium suivantes :

- Indicateurs en temps réel de l'état de la plante.

- Alertes lorsque l'état de la plante n'est pas optimal.

- Des rappels visuels lorsque la plante nécessite d'être arrosée.

Lors de la vente d'une carte à capteurs, 10% du prix revient au partenaire.

### Recyclage

Grâce à l'utilisation de composantes standards, la carte à capteurs peut être
désassemblée et ses composantes réutilisées.
La revente de ces composantes n'est pas une source de revenu réaliste en raison
de leurs bas coût et de la dépréciation des composantes déjà utilisés.
Ces composantes pourront par contre être revendues afin de réduire leur
empreinte environnementalle.

## Communauté

_Greenzz community_, une communauté regroupant des utilisateurs expérimentés
ainsi que des débutants.

Notre stratégie pour amener des utilisateurs expérimentés est divisé en deux
parties :

- Onboarding : L'utilisateur installe l'application et l'utilise en tant que
  journal d'évolution de ses plantes. Il prend des photos journalières de ses
  plantes et s'informe sur leurs caractéristiques détaillées.

- Publish : L'utilisateur est invité à visiter la communauté afin de voir des
  profils de plantes d'autres utilisateurs. Il est de plus aussi invité à
  publier les profils de ses propres plantes et à participer aux concours de
  popularité afin de gagner des bons valables aux magasins de jardinage locaux.

Notre stratégie pour amener des utilisateurs moins expérimentés est également
segmentée :

- Onboarding : L'utilisateur installe l'application afin de parcourir les
  profils des plantes d'autres utilisateurs ou de faire un suivi d'une ou
  plusieurs plantes, configurer des rappels et optionnellement installer une
  carte à capteurs afin de faire un suivi plus détaillé selon les
  recommandations de nos partenaires au moment de l'achat d'une plante.

- Learning : L'utilisateur est invité à demander de l'aide sur l'espace
  communautaire et à explorer les profils de plantes d'autres utilisateurs
  dans la région.

Dans les deux cas, les gens pourront entamer des discussions sur chaque photo ou
créer des groupes de discussion sur un ou plusieurs profils de plantes.
À l'intérieur de chaque groupe, des activités plus avancées (telles que le
classement des profils) seront possibles. Certains de ces groupes seront promus
en vedette sur l'espace communautaire (potentiellement avec intervention de
notre équipe dans le cas de compétitions).

## Partenariats

Nos partenaires pourront annoncer leurs produits au sein de notre application
en fonction du nombre d'utilisateurs visés et la gamme de produits à annoncer.

### Gammes

- « _entry_ » (75.- CHF / mois) permet de proposer un maximum de 10 produits
  à _50_ affichages uniques par jour.
- « _standard_ » (200.- CHF / mois) permet de proposer un maximum de 20 produits
  à _100_ affichages uniques par jour.
- « _gold_ » (300.- CHF / mois) permet de proposer toute la gamme de produits
  à _500_ affichages uniques par jour.

# Présentation du prototype

![Page d'accueil de Greenzz](images/greenzz-homescreen.png)

Lorsque l'utilisateur ouvre l'application pour la première fois, il sera invité
à créer un profil de plante.
Ceci est le premier écran que l'utilisateur voit et définit l'identité visuelle
que les utilisateurs associent à l'application ainsi qu'un premier avis sur
l'ergonomie ou la maturité de celle-ci.
À noter que l'aspect incomplet du texte affiché sur la capture d'écran est
dû à l'animation du texte qui engage l'attention de l'utilisateur.

![Profil de plante sur Greenzz](images/greenzz-profil-plante.jpg)

Lorsqu'un profil de plante est créé, on peut prendre une photo par jour afin
de journaliser l'évolution de notre plante.
Cet écran nous propose également quelques informations en rapport avec l'espèce
de la plante.

![Carte à capteurs](images/sb-dos.png)

![Carte à capteurs installé sur un pot](images/sb-installée.jpg)

![Données en temps réel sur Greenzz](images/greenzz-profil-live.jpg)

# Schéma des processus

Etant donné que notre entreprise fournit surtout un service et ne controlle pas le processus d'achat par l'utilisateur (il utilise le système de payement intégré aux différents magasin d'application pour utiliser le mode payant de l'application et la vente de carte capteur est déléguée aux différents magasins partenaires), il ne parrait pas pertinent de faire un diagramme des processus. Il parrait beaucoups plus interressent de faire un schéma qui montre comment l'utilisateur final évolue dans sa consomation de notre produit au fil du temps.

![évolution utilisateur](images/processusClient.png)

# Produit

## Stratégie commerciale

### Attraction des clients

#### Utilisateurs

Afin de créer la communauté, on prévoit de cibler un public jeune qui est
beaucoup plus susceptible d'essayer l'application et d'interagir dans l'espace
communautaire.
Bien que ce public ait peu de chances de générer des revenus directs, il
pourra démarrer une popularisation par bouche à oreille et nous fournir du
contenu pour démarrer la communauté.
Ces formes de publicité pourront être réduites lorsque la taille de la
communauté aura augmenté.

- Promotion de la marque sur les stores d'applications et chez des
  _influenceurs_ des réseaux sociaux.

- Publicité auprès des universités.

Afin d'atteindre finalement notre publique cible, nous pourrons placer des
publicités de manière plus stratégique.

- Promotion de l'application chez les magasins partenaires.

- Promotion de l'application aux marchés botaniques.

Dans un deuxième temps, lorsque la communauté aura atteint environ 5000
utilisateurs inscrits ou 500 profils de plantes partagés, nous pourrons
commencer à faire de la publicité pour un public plus large, par exemple sur
les réseaux sociaux.

#### Partenaires

Dans un premier temps, nous pourrons proposer de partenariats « _early access_ »
où les produits de nos partenaires seront affichés dans une section de l'espace
communautaire, en échange de quoi ils feront de la publicité pour notre
application chez leurs clients.

Une fois que la communauté aura pris plus de traction, nous pourrons utiliser
les données de fréquentation (e.g. combien d'utilisateurs par jour) recueillies
afin de convertir les partenariats « _early access_ » en partenariats _entry_,
_standard_ ou _gold_ où les magasins auront leurs produits affichés à
différentes quantités de personnes.

### Preuves de traction

Nous avons présenté le concept ainsi qu'un prototype (sans l'aspect
communautaire) à une population adulte jeune dans notre entourage, qu'ils ont
essayé pendant quelques jours.
Lors de ces expériences, les participants manifestent être engagé d'un point de
vue émotionnel et qu'ils aimeraient énormément pouvoir partager avec leurs
entourage.
Leurs témoignage nous indique des très fortes chances d'une expansion rapide
lors de la phase de croissance de la communauté.

Les participants nous ont aussi exprimer que notre application leurs donnerait
la confiance d'essayer d'élever des plantes avec des besoins plus particuliers
qui seraient autrement trop compliqué ou chronophage de faire de leurs propre
soins.

- Croissance de la communauté.
- Un nombre important de profils de plantes enregistrés.
- Visibilité sur les réseaux sociaux.
- 100,000 téléchargements de l'application.

### Vitesse de croissance

Pendant la première année, nous avons décidé de nous concentrer sur la création
de la communauté ainsi que sur l'expérience utilisateur de l'application.
Nous avons ainsi décidé de reporter le lancement de la carte à capteurs.
Après quelques mois, nous espérons atteindre un volume communautaire important
de sorte à limiter les dépenses en publicité et changer l'objectif de croissance
de la communauté par le lancement de la carte à capteurs auprès nos partenaires
et motiver les utilisateurs existants sur l'application à découvrir d'autres
plantes nécessitant potentiellement des soins plus précis, les encourageant à
acquérir une carte à capteurs pour leurs nouvelles plantes.

# Finances

## Budget prévisionnel
Nous allons nous projeter dans l'aspect financier des trois premières années après le lancement de notre produit.

### Chiffre d'affaires
L'objectif de la première année étant axé sur la création et le développement de l'aspect communautaire de l'application, nous ne prévoyons pas d'obtenir un chiffre d'affaires significatif pour cette année-là. Ce chiffre augmentera cependant lors de la deuxième et troisième année en supposant qu'une communauté suffisamment conséquente et ancrée se soit constituée. Nous pouvons ainsi estimer 50000 utilisateurs, dont un tiers possédant un abonnement Pro. Ce dernier coûtant 10 CHF, cela correspondrait à un revenu de 160'000 CHF. Additionnellement, nous pouvons estimer qu'approximativement 5'000 carte à capteurs auraient trouvé preneur. Chaque carte coûtant 30 CHF, le revenu dû aux ventes de ces derniers reviendrait à 150'000 CHF.

#### Matières premières
Notre produit ne nécessite pas d'utilisation de matières premières directement. Nous pouvons cependant compter le coût des livraisons des cartes à capteurs, car le transport consomme du pétrole. En supposant que nous commanderions l'équivalent de 30kg de capteurs par année à compter de la 2ème année après lancement du produit, nous obtenons un montant approximatif de 100 CHF. Cela reviendrait à environ 50 CHF par cargaison via la poste.

#### Salaires et charges sociales
Nous aurons besoin de former une équipe de 5 personnes: un chef de projet, trois développeurs ainsi qu'un community manager. Le chef de projet gagne en moyenne 100'000 CHF/an, les développeurs environ 90'000 CHF/an et le community manager 24'000 CHF/an car il travaille a 50%.
Les charges sociales s'élèvent à approximativement 12% du salaire brut de chacun. En additionnant la totalité de ces montants, nous obtenons 441'280 CHF. En 3 ans, cela correspondrait donc à 1'323'840 CHF.

#### Locations de surfaces
Au cours des 3 années, nous aurons besoin de suffisamment d'espace pour à la fois offrir des bureaux de travail pour les employés ainsi que pour stocker le matériel destiné aux clients, c'est-à-dire les cartes à capteurs.
Afin de limiter les coûts excessifs, nous aurons uniquement besoin de bureaux lors de la première année. Selon ce que nous avons estimé précédemment, il est nécessaire de louer un espace pouvant accueillir entre 5 à 10 personnes. En visant des locaux d'environ 80-90m^2, le loyer reviendrait à environ 2500-3000 CHF par mois. La somme totale pour cette première année revient donc à  36'000 CHF au maximum. Pour les deux années suivantes, il faudra agrandir les locaux afin de stocker le matériel supplémentaire. En ajoutant une salle aux locaux d'environ 15m2, nous arrivons sur un loyer d'environ 3'500 CHF par mois. Le loyer des deux années combinées est donc de 84'000 CHF.
Au total, le montant pour ces trois années de location s'aditionne à 120'000 CHF.

#### Achat et location de matériel
Notre produit nécessite un développement constant de l'application mobile permettant le suivi de plantes des utilisateurs. Il est donc primordial d'être en possession de matériel approprié. Chaque développeur se verra donc prêter un ordinateur moderne et puissant ainsi que tout autre périphérique au besoin. Nous pouvons donc compter environ 4'000 CHF par employé IT. Les autres employés devant certes également travailler avec des ordinateurs mais moins contraignants, nous pouvons leur allouer un budget de 3'000 CHF.
En nous basant sur les suppositions établies précédemment, cela nous coûterait par conséquent une somme totale de 18'000 CHF.

#### Publicité
Durant la première année, nous nous focaliserons principalement sur la publicité en ligne. Nous pouvons compter environ 20.-/jour pour la publicité sur les réseaux sociaux. En choisissant 4 mois complets pour cette dernière, cela nous coûterait 2'400 CHF au total.
Durant les deux prochaines années, nous pourrons diminuer la fréquence de la publicité en ligne et la remplacer par des panneaux publicitaires. En optant pour une campagne sur réseaux sociaux de 2 mois, cela nous reviendrait à 1'200 CHF par année. Cela nous permettrait de louer un panneau d'affichage physique pour un mois. A environ 1'000 CHF/semaine, la somme sera donc de 4'000 CHF.
Tout au long des années, nous afficherons également des flyers à afficher aux universités ou autre endroit attirant les jeunes adultes. En estimant une quantité de 5'000 feuilles, le prix de l'impression s'élèverait à environ 170 CHF.
Le montant total nécessaire à tous ces types de publicités confondues revient par conséquent à 7'770 CHF.

#### Prêt-intérêts
Nous devons emprunter une somme de 250'000 CHF afin de pouvoir avoir suffisamment d'argent pour payer les employés, faire de la publicité pour le produit ainsi que de stocker une certaine quantité de cartes à capteur à vendre.

#### Investissements initiaux
Afin de pouvoir débuter la création de notre produit, nous devons obligatoirement investir dans la production des cartes à capteur. Nous devons compter un investissement d'environ 10'000 CHF chez un producteur qui assemblerait tous les capteurs nécessaires sur une seule carte.
