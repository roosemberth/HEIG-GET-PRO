We use technology to bring us closer to our nature.

Vous aimez les plantes mais vous n'avez pas forcément la main verte ?
Êtes vous un passioné des plantes et vous aimeriez partager vos créations avec d'autres amateurs de plantes ?
Fournir une solution aux personnes maladroites qui oublient d'arroser leurs plantes.

On utilise les outils technologiques pour nous rapprocher de la nature.

