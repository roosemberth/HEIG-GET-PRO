# Project de groupe pour le cours de GET

Ce projet utilise ce template pour compiler le rapport: https://gitlab.com/roosemberth/code-report-ConTeXt-env
Voir ce projet pour avoir les instructions sur comment build et compiler.

[PDF de master géneré par la CI](https://gitlab.com/roosemberth/HEIG-GET-PRO/-/jobs/artifacts/master/raw/report.pdf?job=Render%20example).
